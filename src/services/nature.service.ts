import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root',
})
export class NatureService {
  constructor(private httpClient: HttpClient) {}

  getNature(): Observable<any> {
    return this.httpClient.get<any>(environment.baseURL);
  }
}
