import { Component, OnInit } from '@angular/core';
import { NatureService } from 'src/services/nature.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  natureResponse: any;
  constructor(
    private natureService: NatureService,
    private loadingController: LoadingController
  ) {}

  async presentLoading(loading: any) {
    return await loading.present();
  }

  async getNature() {
    const loading = await this.loadingController.create({
      spinner: 'lines',
      message: 'Chargement...',
    });
    this.presentLoading(loading);

    this.natureService.getNature().subscribe((response) => {
      this.natureResponse = response.hits;
      loading.dismiss();
    });
  }

  ngOnInit(): void {
    this.getNature();
  }
}
