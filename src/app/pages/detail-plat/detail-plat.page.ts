import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-plat',
  templateUrl: './detail-plat.page.html',
  styleUrls: ['./detail-plat.page.scss'],
})
export class DetailPlatPage implements OnInit {
  objet: any;

  constructor(private activatedRoute: ActivatedRoute) {}

  getObject() {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.objet = JSON.parse(params['objetJSON']);
      if (params && params['objetJSON']) {
        this.objet = JSON.parse(params['objetJSON']);
      }
    });
  }

  ngOnInit() {
    this.getObject();
  }
}
